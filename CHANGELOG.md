1.0.4

* Add translation for Indonesian (id). Thanks to @shandya
* Add translation for Russian (ru-RU). Thanks to @alla_realization
* Add translation for Russian Marathi (mr-IN). Thanke to @NDesai
* Add automatic alignement. Only text and size needed now.

1.0.3

* Add translation for the Georgian language (ka-GE). Thanks to @lkhintibidze
* Add translation for the Spanish language [Latin-american version]. Thanks to @Nix41
* Add translation for Polish (pl-pl). Thanks to @narvis
* Add translation to German (de). Thanks to @mamumuma
* Add translation for Hungarian (hu). Thanks to @bodnarzs
* Add translation into Romanian (ro). Thanks to @dia.spoiala

  twitter message

* Georgian language (ka-GE) by @lkhintibidze
* Spanish language (es-lat) by @Nix41
* Polish (pl-pl) by @narvis
* German (de) by @mamumuma
* Hungarian (hu) by @bodnarzs
* Romanian (ro) by @dia.spoiala

1.0.2

* Add Brazilian Portuguese (pt-BR) translation. Thanks to @julianosi23
* Add Argentinian Spanish (es-AR) translation. Thanks to @batager
* Add Hindi (hi) translation. Thanks to @parthsane
* Add Arabic (ar-sa) translation. Thanks to @Shaata81
* Add Farsi/Persian (fa-ir). Thanks to @albacore

1.0.1

* Add italian (it-it) translation. Thanks to @Dabolus
* Add Spanish (es-es) translation. Thanks to @msonsona
* Add catalan (ca) translation. Thanks to @msonsona
* Add dutch (nl-nl) translation. Thanks to @yannick1691

1.0.0

* Add Franch (fr-ch) translation.

Common Issues

* Please enable 'Allow commits from members who can merge to the target branch' by 'edit' in the top of this merge request.
* Please try to better align your texts